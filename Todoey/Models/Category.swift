//
//  Category.swift
//  Todoey
//
//  Created by Isizwe Madalane on 2021/07/03.
//

import Foundation
import RealmSwift

class Category: Object {
    @objc dynamic var name: String = ""
    @objc dynamic var color: String = ""
    let items = List<Item>()
}
